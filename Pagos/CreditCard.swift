//
//  CreditCard.swift
//  Pagos
//
//  Created by Diego Seminario on 11/07/16.
//  Copyright © 2016 Diego Seminario. All rights reserved.
//

import UIKit

class CreditCard {
    
    // MARK: Properties
    var institution: String
    var creditCardNumber: String
    var cvc: Int?
    var month: Int?
    var year: Int?
    
    init(institution: String, creditCardNumber: String, cvc: Int = 0, month: Int = 01, year: Int = 2015) {
        self.institution = institution
        self.creditCardNumber = creditCardNumber
        self.cvc = cvc
        self.month = month
        self.year = year
    }
}
