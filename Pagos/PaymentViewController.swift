//
//  PaymentViewController.swift
//  Pagos
//
//  Created by Diego Seminario on 2/08/16.
//  Copyright © 2016 Diego Seminario. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import SwiftLoader

class PaymentViewController: UIViewController {
    
    let creditCardsDropDown = DropDown()
    
    var total: Double = 0.0
    
    @IBOutlet weak var creditCardButton: UIButton!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblIVA: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    @IBAction func pickCreditCard(sender: UIButton) {
        creditCardsDropDown.show()
    }
    
    @IBAction func payShop(sender: UIButton) {
        print("Credit Card Picked -> \(creditCardButton.titleLabel?.text)")
        
        SwiftLoader.show(animated: true)
        
        Alamofire.request(.POST, "http://diegoseminario.com/upc/pos/public/shopping/payment")
            .responseJSON { [weak self] response in
                
                switch response.result {
                case .Success(let JSON):
                    self?.getPayment(JSON)
                default:
                    SwiftLoader.hide()
                }
                
                
        }
    }
    
    func getPayment(data: AnyObject) {
        SwiftLoader.hide()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDropDown()

        // Do any additional setup after loading the view.
        
        let subTotal = total - (total * 0.18)
        let iva = total * 0.18
        
        lblTotal.text = "S/. \(String(format: "%.2f", total))"
        lblSubtotal.text = "S/. \(String(format: "%.2f", subTotal))"
        lblIVA.text = "S/. \(String(format: "%.2f", iva))"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpDropDown() {
        creditCardsDropDown.anchorView = creditCardButton
        creditCardsDropDown.bottomOffset = CGPoint(x: 0, y: creditCardButton.bounds.height)
        
        var dropdown:Array<String> = []
        let creditCards = POSUtil.sharedInstance.creditCardList
        
        for creditCard in creditCards {
            print(creditCard.creditCardNumber)
            dropdown.append(creditCard.creditCardNumber)
        }
        
        creditCardsDropDown.dataSource = dropdown
        
        creditCardsDropDown.selectionAction = { [unowned self] (index, item) in
            self.creditCardButton.setTitle(item, forState: .Normal)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
