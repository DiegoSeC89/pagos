//
//  QRCodeReaderViewController.swift
//  Pagos
//
//  Created by Diego Seminario on 15/07/16.
//  Copyright © 2016 Diego Seminario. All rights reserved.
//

import UIKit
import QRCodeReader
import AVFoundation
import SwiftLoader


class QRPOSViewController: UIViewController, QRCodeReaderViewControllerDelegate {
    
    lazy var reader: QRCodeReaderViewController = {
        let builder = QRCodeViewControllerBuilder { builder in
            builder.reader          = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
            builder.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "ShoppingListSegue") {
            if let destinationVC = segue.destinationViewController as? ShoppingListTableViewController {
                destinationVC.sessionID = "15"
            }
        }
    }
    
    // MARK: - Action
    @IBAction func scanAction(sender: UIButton) {
    
        if QRCodeReader.supportsMetadataObjectTypes() {
            reader.modalPresentationStyle = .FormSheet
            reader.delegate               = self
            
            reader.completionBlock = { (result: QRCodeReaderResult?) in
                if let result = result {
                    print("Completion with result: \(result.value) of type \(result.metadataType)")
                }
            }
            
            presentViewController(reader, animated: true, completion: nil)
        }
        else {
            self.performSegueWithIdentifier("ShoppingListSegue", sender: self)
        }
    }
    
    // MARK: - QRCodeReader Delegate Methods
    
    func reader(reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        self.dismissViewControllerAnimated(true) { [weak self] in
            let shopping = ShoppingListTableViewController()
            shopping.sessionID = result.value
            
            SwiftLoader.show(animated: true)
            
            self?.performSegueWithIdentifier("ShoppingListSegue", sender: self)
        }
        
    }
    
    func readerDidCancel(reader: QRCodeReaderViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
