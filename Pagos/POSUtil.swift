//
//  POSUtil.swift
//  Pagos
//
//  Created by Diego Seminario on 2/08/16.
//  Copyright © 2016 Diego Seminario. All rights reserved.
//

import Foundation

class POSUtil {
    
    static let sharedInstance = POSUtil()
    
    var creditCardList = Array<CreditCard>()
    
    func storeCreditCards(creditCard: CreditCard) {
        creditCardList.insert(creditCard, atIndex: 0)
    }
    
    func getCreditCards() -> Array<CreditCard> {
        return creditCardList
    }
}