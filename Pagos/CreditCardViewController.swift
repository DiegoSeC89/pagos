//
//  CreditCardViewController.swift
//  Pagos
//
//  Created by Diego Seminario on 4/07/16.
//  Copyright © 2016 Diego Seminario. All rights reserved.
//

import Eureka
import EurekaCreditCard
import Alamofire
import SwiftLoader

class CreditCardViewController: FormViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        weak var weakSelf = self
        
        print("CreditCardViewController init")
        
        form +++ Section("Credit Card Information")
            <<< ActionSheetRow<String>() { row in
                row.title = "Institution"
                row.selectorTitle = "Pick an Institution"
                row.options = ["Ripley", "BCP", "Continental", "Interbank"]
                row.value = "Ripley"
                row.tag = "institution"
        }
            <<< CreditCardRow() { row in
                row.expirationYearPlaceholder = "YY"
                row.expirationMonthPlaceholder = "MM"
                row.cvcPlaceholder = "CVC"
                row.tag = "cardData"
                row.value = EurekaCreditCard.CreditCard()
                
        }
        
            <<< ButtonRow() { row in
                row.title = "Save"
                
                } .onCellSelection { cell, row in
                    weakSelf?.saveCreditCard()
        }
        
        
    }
    
    private func saveCreditCard() {
        SwiftLoader.show(animated: true)
        
        weak var weakSelf = self
        let valuesDictonary = form.values()
        
        let row: CreditCardRow? = form.rowByTag("cardData")
        let value = row!.value
        
        let institution = valuesDictonary["institution"] ?? ""
        let creditCardNumber = value?.cardNumber ?? ""
        let cvc = value?.cvc ?? ""
        let month = value?.expirationMonth ?? ""
        let year = value?.expirationYear ?? ""
        
        let params: [String: AnyObject] = [
            "institution": institution as! AnyObject,
            "creditCardNumber": creditCardNumber,
            "cvc": cvc,
            "month": month,
            "year": year
        ]

        
        Alamofire.request(.POST, "http://diegoseminario.com/upc/pos/public/creditCard", parameters: params)
            .responseJSON { response  in
                
                switch response.result {
                case .Success(let JSON):
                    weakSelf?.successSave(JSON)
                default:
                    print("No status")
                }
        }
    }
    
    private func successSave(json: AnyObject) {
        let data = json as! NSDictionary
        let response = data.objectForKey("response") as! NSDictionary
        
        let creditCard = CreditCard(
            institution: response.objectForKey("institution") as! String,
            creditCardNumber: response.objectForKey("creditCardNumber") as! String
        )
        
        if let controller = self.navigationController?.viewControllers[0] as? ListCreditCardTableViewController {
            controller.creditCardList.insert([creditCard], atIndex: 0)
            SwiftLoader.hide()
            navigationController?.popToRootViewControllerAnimated(true)
        }
        
    }
    
}
