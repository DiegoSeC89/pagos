//
//  Product.swift
//  Pagos
//
//  Created by Diego Alexis Seminario Capullan on 8/1/16.
//  Copyright © 2016 Diego Seminario. All rights reserved.
//

import Foundation

class Product {
    
    // MARK: Properties
    
    var name: String
    var price: Double
    var quantity: Int
    
    init(name: String, price: Double, quantity: Int) {
        self.name = name
        self.price = price
        self.quantity = quantity
    }
}