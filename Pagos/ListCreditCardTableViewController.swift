//
//  ListCreditCardTableViewController.swift
//  Pagos
//
//  Created by Diego Seminario on 4/07/16.
//  Copyright © 2016 Diego Seminario. All rights reserved.
//

import UIKit
import SwiftLoader
import Alamofire

class ListCreditCardTableViewController: UITableViewController {
    
    var creditCardList = [Array<CreditCard>]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    var isCalling = false

    override func viewDidLoad() {
        super.viewDidLoad()
        print("List CreditCard Controller")
        getCreditCards()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return creditCardList.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return creditCardList[section].count
    }
    
    private func getCreditCards() {
        SwiftLoader.show(animated: true)
        
        isCalling = true
        weak var weakSelf = self
        
        Alamofire.request(.GET, "http://diegoseminario.com/upc/pos/public/creditCard")
            .responseJSON { response in
                
//                if let json = response.result.value {
//                    print("JSON: \(json)")
//                }
                
                switch response.result {
                case .Success(let JSON):
                    let response = JSON as! NSDictionary
                    weakSelf?.setData(response.objectForKey("response")!)
                default:
                    SwiftLoader.hide()
                }
                
                
                
        }
    }
    
    private func setData(creditCards: AnyObject) {
        SwiftLoader.hide()
        
        let creditCardArray = creditCards as! NSArray
        var dataArray = [CreditCard]()
        
        for item in creditCardArray {
            let data = item as! NSDictionary
            
            let creditCard = CreditCard(
                institution: data.objectForKey("institution") as! String,
                creditCardNumber: data.objectForKey("creditCardNumber") as! String
            )
            
            POSUtil.sharedInstance.storeCreditCards(creditCard)
            
            dataArray.append(creditCard)
        }
        
        creditCardList.insert(dataArray, atIndex: 0)
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CreditCardCell", forIndexPath: indexPath)

        // Configure the cell...
        let creditCard = creditCardList[indexPath.section][indexPath.row]
        
        cell.textLabel?.text = creditCard.creditCardNumber
        cell.detailTextLabel?.text = creditCard.institution

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
