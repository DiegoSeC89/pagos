//
//  ShoppingListTableViewController.swift
//  Pagos
//
//  Created by Diego Seminario on 17/07/16.
//  Copyright © 2016 Diego Seminario. All rights reserved.
//

import UIKit
import PusherSwift
import SwiftLoader
import SCLAlertView

class ShoppingListTableViewController: UITableViewController, ConnectionStateChangeDelegate {

    var sessionID: String?
    
    let pusher = Pusher(key: "b0b8da799a9dda7e42b8")
    
    var shoppingList = [Array<Product>]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    var total = 0.00
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weak var weakSelf = self
        
        print("subscribe")
        
        let channel = pusher.subscribe("shopping-channel")
        
        print("Channel \(channel.subscribed)")
        
        channel.bind("product", callback: { (data: AnyObject?) -> Void in
            print("Data recieved")
            weakSelf?.updateShoppingList(data)
        })
        
        // remove the debugLogger from the client options if you want to remove the
        // debug logging, or just change the function below
        let debugLogger = { (text: String) in debugPrint(text) }
        pusher.connection.debugLogger = debugLogger
        
        pusher.connection.stateChangeDelegate = self
        
        pusher.bind({ (message: AnyObject?) in
            if let message = message as? [String: AnyObject], eventName = message["event"] as? String where eventName == "pusher:error" {
                if let data = message["data"] as? [String: AnyObject], errorMessage = data["message"] as? String {
                    print("Error message: \(errorMessage)")
                }
            }
        })
        
        pusher.connect()
        
        print("Session ID -> \(sessionID)")
        
        SwiftLoader.hide()
    }
    
    override func viewWillDisappear(animated: Bool) {
        pusher.unsubscribe("shopping-channel")
        pusher.disconnect()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return shoppingList.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return shoppingList[section].count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("ShoppingCell", forIndexPath: indexPath)
        
        // Configure the cell...
        let product = shoppingList[indexPath.section][indexPath.row]
        
        cell.textLabel?.text = product.name
        cell.detailTextLabel?.text = String(product.price)
        
        return cell
    }
    
    func updateShoppingList(data: AnyObject?) {
        if let action = data?.objectForKey("action") as? String {
            switch action {
            case "add":
                addProduct(data!.objectForKey("product")!)
            case "finish":
                showAlert()
            default:
                print("nothing")
            }
        }
    }
    
    func addProduct(data: AnyObject) {
        var dataArray = [Product]()
        
        let product = Product(
            name: data.objectForKey("name") as! String,
            price: data.objectForKey("price") as! Double,
            quantity: data.objectForKey("quantity") as! Int
        );
        
        dataArray.append(product)
        
        shoppingList.insert(dataArray, atIndex: 0)
        
        total += product.price
        
        print("Adding product \(product.name) -> \(product.price)")
    }
    
    func showAlert() {
        
        weak var weakSelf = self
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        
        let alertView = SCLAlertView(appearance: appearance)
    
        alertView.addButton("Pay", action: {
            weakSelf?.performSegueWithIdentifier("PaymentDetailSegue", sender: weakSelf)
        })
        
        alertView.showInfo("Shopping Details", subTitle: "The amount you have to pay is: S/. \(total)")
    }
    
    func connectionChange(old: ConnectionState, new: ConnectionState) {
        print("old: \(old) -> new: \(new)")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PaymentDetailSegue" {
            let paymentView = segue.destinationViewController as! PaymentViewController
            paymentView.total = total
        }
    }

}
